package com.accenture.camel.caller;

import java.util.ArrayList;
import java.util.List;

public class CuentaModels {

	private List<CuentaModel> Cuentas = new ArrayList<>();

	public List<CuentaModel> getCuentas() {
		return Cuentas;
	}

	public void setCuentas(List<CuentaModel> cuentas) {
		Cuentas = cuentas;
	}

}
