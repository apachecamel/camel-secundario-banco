package com.accenture.camel.caller;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Banco {
	
	@Id
	@GeneratedValue
	private Long id;
	private String alias;
	private double fondos;
	private int nroclientes;
	
	public int getNroclientes() {
		return nroclientes;
	}
	public void setNroclientes(int nroclientes) {
		this.nroclientes = nroclientes;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getAlias() {
		return alias;
	}
	public void setAlias(String alias) {
		this.alias = alias;
	}
	public double getFondos() {
		return fondos;
	}
	public void setFondos(double fondos) {
		this.fondos = fondos;
	}

}
