package com.accenture.camel.caller;

import java.util.Date;

public class CuentaModel {
	
	private Long id;
	private String name;
	private int categoria;
	private double saldo;
	private Date date;
	private Long idbanco;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getCategoria() {
		return categoria;
	}
	public void setCategoria(int categoria) {
		this.categoria = categoria;
	}
	public double getSaldo() {
		return saldo;
	}
	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Long getIdbanco() {
		return idbanco;
	}
	public void setIdbanco(Long idbanco) {
		this.idbanco = idbanco;
	}
}
