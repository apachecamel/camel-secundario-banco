package com.accenture.camel.caller;

import org.springframework.data.jpa.repository.JpaRepository;

public interface BancoDao extends JpaRepository<Banco, Long> {
	
	public Banco findByAlias(String alias);

}
