package com.accenture.camel.caller;

import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.jackson.JacksonDataFormat;
import org.apache.camel.component.jackson.ListJacksonDataFormat;
import org.apache.camel.json.simple.JsonObject;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.apache.camel.model.rest.RestBindingMode;
import org.apache.camel.util.toolbox.AggregationStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import com.accenture.camel.caller.*;

@Component
public class Router extends RouteBuilder {

	private JacksonDataFormat formato = new ListJacksonDataFormat(CuentaModel.class);

	@Autowired
	private BancoDao bancodao;

	@Override
	public void configure() throws Exception {
		
		/**
		 *  ?bridgeEndpoint=true (default : false)
		 *  
		 *  si esta en true, HttpProducer ignora el header Exchange.HTTP_URI y utiliza la direccion del endpoint
		 *  
		 *  si throwExceptionOnFailure esta en false, HttpProducer retorna todos los fallos al caller
		 * 
		 */

		restConfiguration().component("servlet").port(8080).host("localhost").bindingMode(RestBindingMode.json);

		rest("/proxy")

				.put("/inducir/").route().setHeader(Exchange.HTTP_METHOD).constant(HttpMethod.GET)
				.to("http://localhost:9090/cuenta?bridgeEndpoint=true").unmarshal(formato).split().simple("${body}")
				.process(new Processor() {

					@Override
					public void process(Exchange exchange) throws Exception {

						System.out.println("Llega al procesador...");

						CuentaModel cuenta = exchange.getIn().getBody(CuentaModel.class);

						if (cuenta.getIdbanco() == null) {

							if (cuenta.getSaldo() >= 45000) {

								cuenta.setIdbanco((long) 1);

							} else if (cuenta.getSaldo() >= 25000) {

								cuenta.setIdbanco((long) 2);

							} else if (cuenta.getSaldo() >= 5000) {

								cuenta.setIdbanco((long) 3);
							}

							if (cuenta.getIdbanco() != null) {

								Banco banco = bancodao.findById(cuenta.getIdbanco()).orElse(null);

								if (banco != null) {

									banco.setNroclientes(banco.getNroclientes() + 1);

									System.out.println(
											"Bienvenido a " + banco.getAlias().toUpperCase() + ", " + cuenta.getName());

									cuenta.setName(cuenta.getName() + " (" + banco.getAlias().toUpperCase() + ")");

									bancodao.save(banco);

								}

							}

						}
					}
				}).setHeader(Exchange.HTTP_METHOD).constant(HttpMethod.POST).marshal().json(JsonLibrary.Jackson)
				.log("${body}").to("http://localhost:9090/cuenta/single?bridgeEndpoint=true").endRest()

				//solo para testeo, induce cuentas a banco galicia
				.put("/inducirgalicia").route().setHeader(Exchange.HTTP_METHOD).constant(HttpMethod.GET)
				.to("http://localhost:9090/cuenta?bridgeEndpoint=true").unmarshal(formato).split().simple("${body}")
				.choice().when().simple("${body.saldo} >= 45000").process(new Processor() {

					@Override
					public void process(Exchange exchange) throws Exception {

						CuentaModel cuenta = exchange.getIn().getBody(CuentaModel.class);

						Banco galicia = bancodao.findByAlias("galicia");

						if (cuenta.getIdbanco() == null) {

							galicia.setNroclientes(galicia.getNroclientes() + 1);

							System.out.println("Bienvenido a Galicia: " + cuenta.getName());

							cuenta.setName(cuenta.getName() + " (GALICIA)");

							cuenta.setIdbanco((long) 1);

							bancodao.save(galicia);

						}

						exchange.getOut().setBody(cuenta);
					}
				}).otherwise().log("Perdon, no cumple con los fondos necesarios").end().end().log("${body}")
				.setHeader(Exchange.HTTP_METHOD).constant(HttpMethod.POST).marshal().json(JsonLibrary.Jackson)
				.to("http://localhost:9090/cuenta?bridgeEndpoint=true").unmarshal().json(JsonLibrary.Jackson).endRest()

				//solo para testing, induce cuentas a banco nacion
				.put("/inducirnacion").route().setHeader(Exchange.HTTP_METHOD).constant(HttpMethod.GET)
				.to("http://localhost:9090/cuenta?bridgeEndpoint=true").unmarshal(formato).split().simple("${body}")
				.choice().when().simple("${body.saldo} >= 5000").process(new Processor() {

					@Override
					public void process(Exchange exchange) throws Exception {

						CuentaModel cuenta = exchange.getIn().getBody(CuentaModel.class);

						Banco nacion = bancodao.findByAlias("nacion");

						if (cuenta.getIdbanco() == null) {

							nacion.setNroclientes(nacion.getNroclientes() + 1);

							System.out.println("Bienvenido a Nacion: " + cuenta.getName());

							cuenta.setName(cuenta.getName() + " (NACION)");

							cuenta.setIdbanco((long) 3);

							bancodao.save(nacion);

						}

						exchange.getOut().setBody(cuenta);

					}
				}).otherwise().log("Perdon, no cumple con los fondos necesarios").end().end().log("${body}")
				.setHeader(Exchange.HTTP_METHOD).constant(HttpMethod.POST).marshal().json(JsonLibrary.Jackson)
				.to("http://localhost:9090/cuenta?bridgeEndpoint=true").unmarshal().json(JsonLibrary.Jackson).endRest()

				//solo para testing, induce cuentas a banco city
				.put("/inducircity").route().setHeader(Exchange.HTTP_METHOD).constant(HttpMethod.GET)
				.to("http://localhost:9090/cuenta?bridgeEndpoint=true").unmarshal(formato).split().simple("${body}")
				.choice().when().simple("${body.saldo} >= 25000").process(new Processor() {

					@Override
					public void process(Exchange exchange) throws Exception {

						CuentaModel cuenta = exchange.getIn().getBody(CuentaModel.class);

						Banco city = bancodao.findByAlias("city");

						if (cuenta.getIdbanco() == null) {

							city.setNroclientes(city.getNroclientes() + 1);

							System.out.println("Bienvenido a City: " + cuenta.getName());

							cuenta.setName(cuenta.getName() + " (CITY)");

							cuenta.setIdbanco((long) 2);

							bancodao.save(city);

						}

						exchange.getOut().setBody(cuenta);
					}
				}).otherwise().log("Perdon, no cumple con los fondos necesarios").end().end().log("${body}")
				.setHeader(Exchange.HTTP_METHOD).constant(HttpMethod.POST).marshal().json(JsonLibrary.Jackson)
				.to("http://localhost:9090/cuenta?bridgeEndpoint=true").unmarshal().json(JsonLibrary.Jackson).endRest()

				// postea una unica cuenta con id banco galicia (falta cambiar por UUID/GUID o
				// string mas tarde)
				.post("/galiciapost").consumes(MediaType.APPLICATION_JSON_VALUE).type(CuentaModel.class).route()
				.process(new Processor() {

					@Override
					public void process(Exchange exchange) throws Exception {

						CuentaModel cuenta = exchange.getIn().getBody(CuentaModel.class);

						Banco galicia = bancodao.findByAlias("galicia");

						cuenta.setIdbanco((long) 1);

						galicia.setNroclientes(galicia.getNroclientes() + 1);

					}
				}).setHeader(Exchange.HTTP_METHOD).constant(HttpMethod.POST).marshal().json(JsonLibrary.Jackson)
				.log("${body}").to("http://localhost:9090/cuenta/single?bridgeEndpoint=true").unmarshal()
				.json(JsonLibrary.Jackson).endRest()

				// le saca 1% de saldo a todas las cuentas que pertenecen al banco galicia y lo
				// agrega al galicia
				// puro hardcode en nombres y demas, falta modularizar por ID y que se pase
				// parametro del ID local
				.put("/galiciatax").produces(MediaType.APPLICATION_JSON_VALUE).route().setHeader(Exchange.HTTP_METHOD)
				.constant(HttpMethod.GET).to("http://localhost:9090/cuenta?bridgeEndpoint=true").unmarshal(formato)
				.split().simple("${body}").choice().when().simple("${body.idbanco} == 1").process(new Processor() {

					@Override
					public void process(Exchange exchange) throws Exception {

						CuentaModel cuentaglc = exchange.getIn().getBody(CuentaModel.class);

						Banco galicia = bancodao.findByAlias("galicia");

						double tax = cuentaglc.getSaldo() * 0.01;

						galicia.setFondos(galicia.getFondos() + tax);

						cuentaglc.setSaldo(cuentaglc.getSaldo() - tax);

						exchange.getOut().setBody(cuentaglc);

						bancodao.save(galicia);

					}
				}).setHeader(Exchange.HTTP_METHOD).constant(HttpMethod.POST).marshal().json(JsonLibrary.Jackson)
				.log("${body}").to("http://localhost:9090/cuenta/single?bridgeEndpoint=true").otherwise()
				.log("no permitido").end().end().endRest()

				//tax para 1 banco, dado un id, galicia = 1 ; city = 2 ; nacion = 3
				.put("/{id}/tax").route().log("${header.id}").setHeader(Exchange.HTTP_METHOD).constant(HttpMethod.GET)
				.toD("http://localhost:9090/cuenta?bridgeEndpoint=true").unmarshal(formato).split().simple("${body}")
				.log("${header.id}").choice().when().simple("${body.idbanco} == ${header.id}").process(new Processor() {

					@Override
					public void process(Exchange exchange) throws Exception {

						CuentaModel cuenta = exchange.getIn().getBody(CuentaModel.class);

						System.out.println("el id del header es:");

						System.out.println(exchange.getIn().getHeader("id"));

						Banco banco = bancodao.findById(cuenta.getIdbanco()).orElse(null);

						if (banco != null) {

							double tax = cuenta.getSaldo() * 0.01;

							banco.setFondos(banco.getFondos() + tax);

							cuenta.setSaldo(cuenta.getSaldo() - tax);

							exchange.getOut().setBody(cuenta);

							bancodao.save(banco);
						}

					}
				}).setHeader(Exchange.HTTP_METHOD).constant(HttpMethod.POST).marshal().json(JsonLibrary.Jackson)
				.log("${body}").to("http://localhost:9090/cuenta/single?bridgeEndpoint=true").otherwise()
				.log("no permitido").end().end().endRest()

				//tax general, usar este
				.put("/tax").route().setHeader(Exchange.HTTP_METHOD).constant(HttpMethod.GET)
				.to("http://localhost:9090/cuenta?bridgeEndpoint=true").unmarshal(formato).split().simple("${body}")
				.process(new Processor() {

					@Override
					public void process(Exchange exchange) throws Exception {

						CuentaModel cuenta = exchange.getIn().getBody(CuentaModel.class);

						if (cuenta.getIdbanco() != null) {

							Banco banco = bancodao.findById(cuenta.getIdbanco()).orElse(null);

							if (banco != null) {

								double tax = cuenta.getSaldo() * 0.01;

								banco.setFondos(banco.getFondos() + tax);

								cuenta.setSaldo(cuenta.getSaldo() - tax);

								exchange.getOut().setBody(cuenta);

								bancodao.save(banco);
							}

						}
					}
				}).setHeader(Exchange.HTTP_METHOD).constant(HttpMethod.POST).marshal().json(JsonLibrary.Jackson)
				.log("${body}").to("http://localhost:9090/cuenta/single?bridgeEndpoint=true").end().endRest();

	}

}


/**
 * 				// ver una cuenta por ID, pega en URI dinamica
				.get("/cuenta/{id}").route().log("${header.id}").setHeader(Exchange.HTTP_METHOD)
				.constant(HttpMethod.GET).toD("http://localhost:9090/cuenta/" + "${header.id}" + "?bridgeEndpoint=true")
				.unmarshal().json(JsonLibrary.Jackson).log("response: ${body}").endRest()

				// opera saldo en una cuenta
				.get("/cuenta/{id}/saldo/{monto}").route().log("ID:${header.id}" + "Monto:${header.monto}")
				.setHeader(Exchange.HTTP_METHOD).constant(HttpMethod.PUT).process(new Processor() {

					@Override
					public void process(Exchange exchange) throws Exception {
						// TODO Auto-generated method stub
						JsonObject j = new JsonObject();
						j.put("id", exchange.getIn().getHeader("id"));
						j.put("saldo", exchange.getIn().getHeader("monto"));
						exchange.getIn().setBody(j);

					}

				}).marshal().json(JsonLibrary.Jackson).toD("http://localhost:9090/cuenta/saldo?bridgeEndpoint=true")
				.unmarshal().json(JsonLibrary.Jackson).log("response: ${body}").endRest();
 * 
 */
